unit DigitalPersona.FingerPrint.Reader;

{ Copyright (c) 2015 Ortu Agustin, ortu.agustin@gmail.com
Copyright (c) 2015 Ortu Agustin, https://gitlab.com/u/ortuagustin
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of copyright holders nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL COPYRIGHT HOLDERS OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE. }

interface

uses
  DigitalPersona.FingerPrint, DigitalPersona.Types, DigitalPersona.FingerPrint.Compare, DPFPEngXLib_TLB,
  DPFPShrXLib_TLB, DPFPDevXLib_TLB, System.Classes;

type
  // forward declarations
  TDPFingerPrintReader = class;

  TOnCaptureEvent = procedure(Reader: TDPFingerPrintReader; FingerComparer: IFingerComparer) of object;

  TDPFingerPrintReader = class(TComponent)
  strict private
    FReader: TDPFPCapture;
    FFeatureSet: IDPFPFeatureExtraction;
    procedure OnComplete(Sender: TObject; const ReaderSerNum: WideString; const pSample: IDispatch);
    procedure PrivateOnFingerTouch(Sender: TObject; const ReaderSerNum: WideString);
    procedure PrivateOnFingerGone(Sender: TObject; const ReaderSerNum: WideString);
  private
    FOnStopCapture: TNotifyEvent;
    FOnStartCapture: TNotifyEvent;
    FOnFingerGone: TDPFPCaptureOnFingerGone;
    FOnFingerTouch: TDPFPCaptureOnFingerTouch;
    FOnCaptured: TOnCaptureEvent;
    FAutoStartCapturing: Boolean;
    FReaderPriority: TReaderPriority;
    function GetReaderPriority: TReaderPriority;
    procedure SetReaderPriority(const Value: TReaderPriority);
    procedure SetOnStartCapture(const Value: TNotifyEvent);
    procedure SetOnStopCapture(const Value: TNotifyEvent);
    procedure SetOnCaptured(const Value: TOnCaptureEvent);
    procedure SetAutoStartCapturing(const Value: Boolean);
  protected
    procedure Loaded; override;
    procedure AfterConstruction; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure StartCapture; overload;
    procedure StartCapture(const Priority: TReaderPriority); overload;
    procedure StartCapture(const RunInBackground: Boolean); overload;
    procedure StopCapture;
  published
    property AutoStartCapturing: Boolean read FAutoStartCapturing write SetAutoStartCapturing default False;
    property ReaderPriority: TReaderPriority read GetReaderPriority write SetReaderPriority default rpNormal;
    property OnStartCapture: TNotifyEvent read FOnStartCapture write SetOnStartCapture;
    property OnStopCapture: TNotifyEvent read FOnStopCapture write SetOnStopCapture;
    property OnFingerTouch: TDPFPCaptureOnFingerTouch read FOnFingerTouch write FOnFingerTouch;
    property OnFingerGone: TDPFPCaptureOnFingerGone read FOnFingerGone write FOnFingerGone;
    property OnCaptured: TOnCaptureEvent read FOnCaptured write SetOnCaptured;
  end;

procedure Register;

implementation

uses
  DigitalPersona.FingerPrint.Hashes, Winapi.ActiveX, System.SysUtils;

procedure Register;
begin
  RegisterComponents('DigitalPersona', [TDPFingerPrintReader]);
end;

{ TDPFingerPrint }

procedure TDPFingerPrintReader.AfterConstruction;
begin
  inherited AfterConstruction;
  if csDesigning in ComponentState then
    Exit;

  ReaderPriority := FReaderPriority;
end;

constructor TDPFingerPrintReader.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FReaderPriority := rpNormal;

  if csDesigning in ComponentState then
    Exit;

  FReader := Reader;
  if FReader = NIL then
    raise Exception.Create('No readers connected');

  FFeatureSet := CoDPFPFeatureExtraction.Create;
  FReader.OnComplete := OnComplete;
  FReader.OnFingerTouch := PrivateOnFingerTouch;
  FReader.OnFingerGone := PrivateOnFingerGone;
end;

destructor TDPFingerPrintReader.Destroy;
begin
  // we don't need to free what we created on constructor, they are all interfaces (reference counted)
  // FReader will be freed on the TComponent destructor
  if not(csDesigning in ComponentState) and (FReader <> NIL) then
    StopCapture;
  inherited Destroy;
end;

function TDPFingerPrintReader.GetReaderPriority: TReaderPriority;
begin
  if csDesigning in ComponentState then
    Result :=  FReaderPriority
  else
    Result := TReaderPriority.FromOleEnum(FReader.Priority);
end;

procedure TDPFingerPrintReader.Loaded;
begin
  inherited Loaded;
  if csDesigning in ComponentState then
    Exit;

  if AutoStartCapturing then
    StartCapture;
end;

procedure TDPFingerPrintReader.SetAutoStartCapturing(const Value: Boolean);
begin
  FAutoStartCapturing := Value;
end;

procedure TDPFingerPrintReader.SetOnCaptured(const Value: TOnCaptureEvent);
begin
  FOnCaptured := Value;
end;

procedure TDPFingerPrintReader.SetOnStartCapture(const Value: TNotifyEvent);
begin
  FOnStartCapture := Value;
end;

procedure TDPFingerPrintReader.SetOnStopCapture(const Value: TNotifyEvent);
begin
  FOnStopCapture := Value;
end;

procedure TDPFingerPrintReader.SetReaderPriority(const Value: TReaderPriority);
begin
  if csDesigning in ComponentState then
    FReaderPriority := Value
  else
    FReader.Priority := Value.ToOleEnunm;
end;

procedure TDPFingerPrintReader.StartCapture;
begin
  FReader.StartCapture;
  if Assigned(OnStartCapture) then
    OnStartCapture(Self);
end;

procedure TDPFingerPrintReader.StartCapture(const Priority: TReaderPriority);
begin
  ReaderPriority := Priority;
  StartCapture;
end;

procedure TDPFingerPrintReader.StartCapture(const RunInBackground: Boolean);
begin
  ReaderPriority := rpHigh;
  StartCapture;
end;

procedure TDPFingerPrintReader.StopCapture;
begin
  FReader.StopCapture;
  if Assigned(OnStopCapture) then
    OnStopCapture(Self);
end;

procedure TDPFingerPrintReader.OnComplete(Sender: TObject; const ReaderSerNum: WideString; const pSample: IDispatch);
var
  LFingerComparer: TFingerComparer;
begin
  // do not process if we haven't any event assigned at OnCaptured
  if not(Assigned(FOnCaptured)) then
    Exit;

  // we need a good sample
  if FFeatureSet.CreateFeatureSet(pSample, DataPurposeVerification) <> CaptureFeedbackGood then
    Exit;

  LFingerComparer := TFingerComparer.Create(FFeatureSet.FeatureSet);
  OnCaptured(Self, LFingerComparer);
end;

procedure TDPFingerPrintReader.PrivateOnFingerGone(Sender: TObject; const ReaderSerNum: WideString);
begin
  if Assigned(FOnFingerGone) then
    FOnFingerGone(Sender, ReaderSerNum);
end;

procedure TDPFingerPrintReader.PrivateOnFingerTouch(Sender: TObject; const ReaderSerNum: WideString);
begin
  if Assigned(FOnFingerTouch) then
    FOnFingerTouch(Sender, ReaderSerNum);
end;

end.
