unit DigitalPersona.FingerPrint.Enroll;

{ Copyright (c) 2015 Ortu Agustin, ortu.agustin@gmail.com
Copyright (c) 2015 Ortu Agustin, https://gitlab.com/u/ortuagustin
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of copyright holders nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL COPYRIGHT HOLDERS OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE. }

interface

uses
  DigitalPersona.FingerPrint, DigitalPersona.Types, DPFPCtlXLib_TLB, DPFPDevXLib_TLB, System.Classes, Vcl.Forms,
  Vcl.Controls;

type
  // forward declarations
  TDPFingerPrintEnroll = class;

  TFingerPrintEnrollEvent = procedure(Sender: TDPFingerPrintEnroll; const Finger: TFinger; const FingerPrint: string)
    of object;

  TFingerPrintDeleteEvent = procedure(Sender: TDPFingerPrintEnroll; const Finger: TFinger) of object;

  TFingerPrintAfterExecuteEvent = procedure(Sender: TDPFingerPrintEnroll; const FingerPrints: TFingerPrints;
    const EnrolledFingers: TFingerSet; const Result: Boolean) of object;

  TDPEnrollForm = class(TCustomForm)
  public
    constructor Create(AOwner: TComponent); override;
  end;

  TDPFingerPrintEnroll = class(TComponent)
  strict private
    FForm: TDPEnrollForm;
    FReader: IDPFPCapture;
    FEnrollmentControl: TDPFPEnrollmentControl;
    FEnrolledFingerPrints: TFingerPrints;
    procedure PrivateOnEnroll(ASender: TObject; lFingerMask: Integer; const pTemplate: IDispatch;
      const pStatus: IDispatch);
    procedure PrivateOnDelete(ASender: TObject; lFingerMask: Integer; const pStatus: IDispatch);
    procedure PrivateOnShow(Sender: TObject);
    procedure PrivateOnClose(Sender: TObject; var Action: TCloseAction);
  private
    FOnFingerEnroll: TFingerPrintEnrollEvent;
    FOnFingerPrintDeleted: TFingerPrintDeleteEvent;
    FMaxFingerPrintsAllowed: Integer;
    FAfterExecute: TFingerPrintAfterExecuteEvent;
    procedure SetOnFingerEnroll(const Value: TFingerPrintEnrollEvent);
    procedure SetOnFingerPrintDeleted(const Value: TFingerPrintDeleteEvent);
    function GetEnrolledFingers: TFingerSet;
    function GetMaxFingerPrintsAllowed: Integer;
    procedure SetEnrolledFingers(const Value: TFingerSet);
    procedure SetMaxFingerPrintsAllowed(Value: Integer);
    procedure SetAfterExecute(const Value: TFingerPrintAfterExecuteEvent);
    procedure ResetFingerPrints;
  protected
    procedure AfterConstruction; override;
  public
    constructor Create(AOwner: TComponent); override;
    function Execute(const ClearFingerPrints: Boolean = True): Boolean; overload;
    function Execute(const EnrolledFingers: TFingerSet): Boolean; overload;
    property EnrolledFingers: TFingerSet read GetEnrolledFingers write SetEnrolledFingers;
  published
    property MaxFingerPrintsAllowed: Integer read GetMaxFingerPrintsAllowed write SetMaxFingerPrintsAllowed default 10;
    property OnFingerEnroll: TFingerPrintEnrollEvent read FOnFingerEnroll write SetOnFingerEnroll;
    property OnFingerPrintDeleted: TFingerPrintDeleteEvent read FOnFingerPrintDeleted write SetOnFingerPrintDeleted;
    property AfterExecute: TFingerPrintAfterExecuteEvent read FAfterExecute write SetAfterExecute;
  end;

procedure Register;

implementation

uses
  DigitalPersona.FingerPrint.Hashes, DPFPShrXLib_TLB, Winapi.Windows, System.SysUtils, Vcl.StdCtrls, Vcl.ExtCtrls;

procedure Register;
begin
  RegisterComponents('DigitalPersona', [TDPFingerPrintEnroll]);
end;

{ TDPFingerPrintEnroll }

procedure TDPFingerPrintEnroll.AfterConstruction;
begin
  inherited AfterConstruction;
  if csDesigning in ComponentState then
    Exit;

  FEnrollmentControl := TDPFPEnrollmentControl.Create(Self);
  FEnrollmentControl.MaxEnrollFingerCount := FMaxFingerPrintsAllowed;
  FForm.Caption := EmptyStr;
  FForm.Width := 500;
  FForm.Height := 400;
  FEnrollmentControl.Parent := FForm;
  FEnrollmentControl.Align := alClient;
  FEnrollmentControl.OnEnroll := PrivateOnEnroll;
  FEnrollmentControl.OnDelete := PrivateOnDelete;
  FForm.Position := poScreenCenter;
end;

constructor TDPFingerPrintEnroll.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FMaxFingerPrintsAllowed := MAX_FINGER_COUNT;

  if csDesigning in ComponentState then
    Exit;

  ResetFingerPrints;
  FForm := TDPEnrollForm.Create(AOwner);
  FForm.OnShow := PrivateOnShow;
  FForm.OnClose := PrivateOnClose;
  FReader := Reader.DefaultInterface;
end;

function TDPFingerPrintEnroll.Execute(const EnrolledFingers: TFingerSet): Boolean;
begin
  ResetFingerPrints;
  FEnrollmentControl.EnrolledFingersMask := EnrolledFingers.ToInteger;
  Result := Execute(False);
end;

function TDPFingerPrintEnroll.Execute(const ClearFingerPrints: Boolean): Boolean;
begin
  if ClearFingerPrints then
    ResetFingerPrints;

  if FForm.ShowModal = mrOk then
    Result := True
  else
    Result := False;

  if Assigned(FAfterExecute) then
    FAfterExecute(Self, FEnrolledFingerPrints, GetEnrolledFingers, Result);
end;

function TDPFingerPrintEnroll.GetEnrolledFingers: TFingerSet;
begin
  Result := TFingerSet.FromInteger(FEnrollmentControl.EnrolledFingersMask);
end;

function TDPFingerPrintEnroll.GetMaxFingerPrintsAllowed: Integer;
begin
  if csDesigning in ComponentState then
    Result := FMaxFingerPrintsAllowed
  else
    Result := FEnrollmentControl.MaxEnrollFingerCount;

  if Result > MAX_FINGER_COUNT then
    Result := MAX_FINGER_COUNT;
end;

procedure TDPFingerPrintEnroll.PrivateOnClose(Sender: TObject; var Action: TCloseAction);
begin
  // resume capturing
  if FReader <> NIL then
    FReader.StartCapture;

  Action := caHide;
end;

procedure TDPFingerPrintEnroll.PrivateOnDelete(ASender: TObject; lFingerMask: Integer; const pStatus: IDispatch);
var
  Finger: TFinger;
begin
  Finger := TFinger.FromInteger(lFingerMask);

  FEnrolledFingerPrints[Finger] := EmptyStr;

  if Assigned(FOnFingerPrintDeleted) then
    FOnFingerPrintDeleted(Self, Finger);
end;

procedure TDPFingerPrintEnroll.PrivateOnEnroll(ASender: TObject; lFingerMask: Integer;
  const pTemplate, pStatus: IDispatch);
var
  FingerPrint: string;
  Finger: TFinger;
begin
  Finger := TFinger.FromInteger(lFingerMask);
  FingerPrint := FingerPrintHasher.Encrypt(DPFPTemplate(pTemplate).Serialize);

  FEnrolledFingerPrints[Finger] := FingerPrint;

  if Assigned(FOnFingerEnroll) then
    FOnFingerEnroll(Self, Finger, FingerPrint);
end;

procedure TDPFingerPrintEnroll.PrivateOnShow(Sender: TObject);
begin
  // if we're using a reader we must ensure it stops capturing; otherwise the enrollement controll will not work
  if FReader <> NIL then
    FReader.StopCapture;
end;

procedure TDPFingerPrintEnroll.ResetFingerPrints;
var
  f: TFinger;
begin
  for f := Low(FEnrolledFingerPrints) to High(FEnrolledFingerPrints) do
    FEnrolledFingerPrints[f] := EmptyStr;

  if Assigned(FEnrollmentControl) then
    FEnrollmentControl.EnrolledFingersMask := 0;
end;

procedure TDPFingerPrintEnroll.SetAfterExecute(const Value: TFingerPrintAfterExecuteEvent);
begin
  FAfterExecute := Value;
end;

procedure TDPFingerPrintEnroll.SetEnrolledFingers(const Value: TFingerSet);
begin
  FEnrollmentControl.EnrolledFingersMask := Value.ToInteger;
end;

procedure TDPFingerPrintEnroll.SetMaxFingerPrintsAllowed(Value: Integer);
begin
  if Value > MAX_FINGER_COUNT then
    Value := MAX_FINGER_COUNT;

  if csDesigning in ComponentState then
    FMaxFingerPrintsAllowed := Value
  else
    FEnrollmentControl.MaxEnrollFingerCount := Value;
end;

procedure TDPFingerPrintEnroll.SetOnFingerEnroll(const Value: TFingerPrintEnrollEvent);
begin
  FOnFingerEnroll := Value;
end;

procedure TDPFingerPrintEnroll.SetOnFingerPrintDeleted(const Value: TFingerPrintDeleteEvent);
begin
  FOnFingerPrintDeleted := Value;
end;

{ TDPEnrollForm }

constructor TDPEnrollForm.Create(AOwner: TComponent);
var
  LPanel: TPanel;
  LButton: TButton;
begin
  inherited CreateNew(AOwner);
  LPanel := TPanel.Create(Self);
  LPanel.ShowCaption := False;
  LPanel.Align := alBottom;
  BorderStyle := bsDialog;
  InsertControl(LPanel);

  LButton := TButton.Create(Self);
  LButton.Caption := 'Aceptar';
  LButton.ModalResult := mrOk;
  LButton.Align := alRight;
  LButton.AlignWithMargins := True;
  LPanel.InsertControl(LButton);

  LButton := TButton.Create(Self);
  LButton.Caption := 'Cancelar';
  LButton.ModalResult := mrCancel;
  LButton.Align := alRight;
  LButton.AlignWithMargins := True;
  LPanel.InsertControl(LButton);
end;

end.
